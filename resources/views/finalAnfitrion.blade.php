@extends('layout')

@section('nombre_y_estilos')
    <title>{{ __('Final.TituloPagina') }}_{{ $sala->nombre }}</title>
    <link rel="stylesheet" href="/css/final.css" type="text/css">
@endsection

@section('cuerpo_principal')
    <h1>{{ __('Final.Titulo') }}</h1>
    <h2>{{ __('Final.Temperatura') }}<strong> {{ $data['temperaturaGlobalSala'] }} {{ __('Final.Grados') }} </strong></h2>   
    <h2>{{ __('Final.TopAcciones') }}</h2>
        <table class="tabla-estadisticas">
        <thead>
            <tr>
                <th>{{ __('Final.AccionRealizada') }}</th>
                <th>{{ __('Final.CantidadJugadores') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data['accionesIncorrectas'] as $elemento => $cantidad)
                <tr>
                        <td>{{ $elemento }}</td>
                        <td>{{ $cantidad }} </td>
                </tr>
            @endforeach
        </tbody>
        </table>
    <a class="boton"href="/salas">{{ __('Final.Volver_menu') }}</a>