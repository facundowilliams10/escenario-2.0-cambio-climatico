@extends('layout')

@section('nombre_y_estilos')
    <title>{{ __('Proyecto.Titulo') }}</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/salas.css">
    <script src="js/paw.js"></script>
    <script src="js/app.js"></script>

@endsection

@section('cuerpo_principal')
    <section class="creadorSalas">
        <form action="{{ route('salas.create') }}" method="POST" class="form_crearSala" name="formulario_crear">
            @csrf
            <h2>{{ __('CreadorSalas.Crear') }}</h2>
            <label for="crear_Name" class="label_crear_Name"> {{ __('CreadorSalas.Nombre') }}</label>
            <input type="text" name="sala_nombre" id="crear_Name" placeholder="{{ __('CreadorSalas.Ejemplo') }}">

            {{-- <label for="radio_TiempoJuego" class="label_radio_TJ"> Tiempo de juego:</label> --}}
            {{-- <fieldset class="radio_TiempoJuego">
                <input type="radio" name="sala_tiempoJuego" id="radio_1" value="10">
                <label for="radio_1">10 min</label>
                <input type="radio" name="sala_tiempoJuego" id="radio_2" value="15">
                <label for="radio_2">15 min</label>
                <input type="radio" name="sala_tiempoJuego" id="radio_3" value="20">
                <label for="radio_3">20 min</label>
            </fieldset> --}}
            <label for="crear_TiempoJuego" class="label_radio_TJ"> {{ __('CreadorSalas.Tiempo') }}</label>
            <input type="number" name="sala_tiempoJuego" id="crear_TiempoJuego" placeholder="20" min="10" max="60">
            <p id="text_tiempo">{{ __('CreadorSalas.TiempoMax') }}</p>

            <label for="crear_NumJugadores" class="label_crear_NJ"> {{ __('CreadorSalas.Jugadores') }}</label>
            <input type="number" name="sala_numJugadores" id="crear_NumJugadores" placeholder="8" min="1" max="40">
            <p id="text_numJug">{{ __('CreadorSalas.JugadoresMax') }}</p>

            <label for="radio_DJ" class="label_radio_DJ"> {{ __('CreadorSalas.Dificultad') }}</label>
            <fieldset class="radio_DJ">
                <input type="radio" name="sala_dificultad" id="radio_D_1" value="Facil">
                <label for="radio_D_1">{{ __('CreadorSalas.Facil') }}</label>
                <input type="radio" name="sala_dificultad" id="radio_D_2" value="Media">
                <label for="radio_D_2">{{ __('CreadorSalas.Media') }}</label>
                <input type="radio" name="sala_dificultad" id="radio_D_3" value="Dificil">
                <label for="radio_D_3">{{ __('CreadorSalas.Dificil') }}</label>
                {{-- <a id="dificultad_ayuda">info dificultad</a>--}}
            </fieldset>

            <fieldset class="botonesCrear">
                <button type="button" class="boton" id="boton_cerrar_creador">{{ __('CreadorSalas.Cancelar') }}</button>
                <input type="submit" value="{{ __('CreadorSalas.Crear') }}" class="boton" id="boton_crear">
            </fieldset>

        </form>
    </section>
@endsection
