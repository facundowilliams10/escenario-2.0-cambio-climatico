@extends('layout')

@section('nombre_y_estilos')
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>$titulo</title>
    <link rel="stylesheet" href="/css/pregunta.css">
@endsection

@section('cuerpo_principal')
    <section class="pregunta">
        <h1> <?= $pregunta->enunciado ?> </h1>
        <figure>
            {{-- <source media="(min-width:465px)" srcset="img_white_flower.jpg"> --}}
            <img src="/imagenes_pregunta/pregunta1.png" alt="OCTANO + OXIGENO => DIOXIDO DE CARBONO + VAPOR DE AGUA"
                style="width:auto;">
            <figcaption>Según las acciones la temperatura sube más o menos... </figcaption>
        </figure>
    </section>
    <section class="respuestas">
        <h2>Respuestas posibles</h2>
        <ol>
            <?php foreach ($respuestas as $respuesta) : ?>
            {{-- Se listan las respuestas correspondientes a la pregunta --}}
            <li> <?= $respuesta->texto ?> </li>
            <?php endforeach; ?>
        </ol>
    </section>
@endsection
