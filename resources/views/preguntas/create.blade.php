<!DOCTYPE html>
<html lang={{__("Html.Language")}}>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/pregunta.css">
    <title>{{__("FormularioPreguntas.Titulo")}}</title>
</head>

<body>
    <h2>{{__("FormularioPreguntas.TituloBody")}}</h2>
    <form action="/admin/preguntas" method="POST">
        @csrf

        <fieldset class="radio_DJ">
            <label for="idioma">{{__("FormularioPreguntas.Idioma")}}</label>
            <input type="radio" name="idioma" id="radio_D_2" value="es" checked>
            <label for="radio_D_2">{{__("FormularioPreguntas.Español")}}</label>
            <input type="radio" name="idioma" id="radio_D_1" value="en">
            <label for="radio_D_1">{{__("FormularioPreguntas.Ingles")}}</label>
        </fieldset class="radio_DJ">

        <fieldset>
            <label for="enunciado">{{__("FormularioPreguntas.Enunciado")}}</label>
            <textarea name="enunciado" id="enunciado" cols="30" rows="5">{{__("FormularioPreguntas.PlaceholderEnunciado")}}</textarea>
        </fieldset>

        <fieldset>
            <label for="elemento_asociado">{{__("FormularioPreguntas.Elemento")}}</label>
            <input placeholder="baniera" type="text" id="elemento_asociado" name="elemento_asociado" required
                value="cepillo">
        </fieldset>

        <fieldset>
            <label for="justificacion">{{__("FormularioPreguntas.Justificacion")}}</label>
            <input placeholder="Justificacion" type="text" id="justificacion" name="justificacion" required
                value="{{__("FormularioPreguntas.PlaceholderJustificacion")}}">
        </fieldset>
        

        <fieldset class="radio_DJ">
            <label for="etapa">{{__("FormularioPreguntas.Etapa")}}</label>
            <input type="radio" name="etapa" id="radio_D_1" value="maniana" checked>
            <label for="radio_D_1">{{__("Salas.maniana")}}</label>
            <input type="radio" name="etapa" id="radio_D_2" value="mediodia">
            <label for="radio_D_2">{{__("Salas.mediodia")}}</label>
            <input type="radio" name="etapa" id="radio_D_3" value="tarde">
            <label for="radio_D_3">{{__("Salas.tarde")}}</label>
        </fieldset class="radio_DJ">

        <fieldset class="radio_DJ">
            <label for="dificultad">{{__("FormularioPreguntas.Dificultad")}}</label>
            <input type="radio" name="dificultad" id="radio_D_1" value="Fácil" checked>
            <label for="radio_D_1">{{__("CreadorSalas.Facil")}}</label>
            <input type="radio" name="dificultad" id="radio_D_2" value="Media">
            <label for="radio_D_2">{{__("CreadorSalas.Media")}}</label>
            <input type="radio" name="dificultad" id="radio_D_3" value="Dificil">
            <label for="radio_D_3">{{__("CreadorSalas.Dificil")}}</label>
        </fieldset class="radio_DJ">

        <fieldset>
            <h3>{{__("FormularioPreguntas.Respuesta1")}}</h3>
            <label for="respuesta1">{{__("FormularioPreguntas.Enunciado")}}</label>
            <input type="text" id="respuesta1" name="respuesta1" placeholder="H2" required value={{__("FormularioPreguntas.PlaceholderRespuesta1")}}>
            <label for="grados1">{{__("FormularioPreguntas.Grados")}}</label>
            <input type="number" id="grados1" name="grados1" placeholder="-1" required value=1>
        </fieldset>

        <fieldset>
            <h3>{{__("FormularioPreguntas.Respuesta1")}}</h3>
            <label for="respuesta2">{{__("FormularioPreguntas.Enunciado")}}</label>
            <input type="text" id="respuesta2" name="respuesta2" placeholder="C8H18" required value={{__("FormularioPreguntas.PlaceholderRespuesta1")}}>
            <label for="grados2">{{__("FormularioPreguntas.Grados")}}</label>
            <input type="number" id="grados2" name="grados2" placeholder="-3" required value=3>
        </fieldset>

        <input type="submit" value={{__("CreadorSalas.Guardar")}} class="botonGuardar">
    </form>

</body>

</html>
