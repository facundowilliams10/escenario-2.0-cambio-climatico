@extends('layout')

@section('nombre_y_estilos')
    <title>{{ __('Salas.TituloPagina') }}</title>
    <link rel="stylesheet" href="/css/app.css" type="text/css">
    <link rel="stylesheet" href="/css/salas.css" type="text/css">
    <script src="js/paw.js"></script>
    <script src="js/app.js"></script>
    <script type="application/ld+json">
      {
      "@context": "https://schema.org",
      "@type": "VideoGame",
      "name": "Escenario 2.0",
      "url": "http://34.151.228.178/login",
      "playMode": "Multi-jugador",
      "image": "/imagenes/fondo-original.jpg",
      "description": "Juego enfocado en concientizar sobre el crecimiento de la temperatura del planeta en las actividades diarias.",
      "inLanguage":["Español","English"],
      "translator":[" Google Inc."],
      "author":{
         "@type":"Organization",
         "name":"Universidad Nacional de Luján",
         "url":"http://www.unlu.edu.ar/"
      },
      "publisher":"UNLu",
      "genre":["Trivia, Educación"],
      "quest": [{
         "@type": "Thing",
         "name":"Preguntas Stop",
         "description":"Preguntas que se darán cada cierto tiempo en el juego y reducirán la temperatura global del juego si son las óptimas.",
      },
      {
         "@type": "Thing",
         "name":"Acciones",
         "description":" Representados con objetos a lo largo del mapa, sirven para aumentar el bienestar del jugador además de aumentar temperatura en mayor o menor medida.",
      }],
      "characterAttribute": {
         "@type": "Thing",
         "name":" Medidor de Bienestar",
         "alternateName":"Bienestar",
         "description":"Medidor que mostrar la medida actual del bienestar del individuo, se ira disminuyendo a medida que pase el tiempo, si llega a 0 el jugador pierde."
      },
      "processorRequirements":"1 GHz",
      "memoryRequirements":"2 Gb",
      "storageRequirements":"0 Gb",
      "gamePlatform":["PC", "Smartphone", "Tablet"],
      "gameLocation":["Ciudad", "Escuela", "Casa"]
      "cheatCode":"Contesta las preguntas o realiza las acciones siendo consciente del efecto que causa en el clima"
      }
   </script>
   <script>
    function alternar(check) {
        espaniol.checked = false;
        ingles.checked = false;

        check.checked = true;
        window.location.replace("/language-change/?language=" + check.value);
    }
</script>

@endsection

@section('cuerpo_principal')
    <x-rotar-pantalla /> 
    <h1>{{ __('Proyecto.Titulo') }}</h1>
    <form id="form_lenguaje">
        <h2>Idioma</h2>
        <fieldset>
            <label>{{ __('FormularioPreguntas.Español') }}
                <input id="espaniol" onclick="alternar(this)" type="checkbox" name="language" value="es"
                    {{ App::getLocale() == 'es' ? 'checked' : '' }}>
            </label>

            <label>{{ __('FormularioPreguntas.Ingles') }}
                <input id="ingles" onclick="alternar(this)" type="checkbox" name="language" value="en"
                    {{ App::getLocale() == 'en' ? 'checked' : '' }}>
            </label>
        </fieldset>
    </form>
    {{-- Cartel Bienvenido <usuario> --}}
    @auth
        <x-header-logueado />
    @endauth

    <!-- Logo -->
    <section id="welcome_title">
        <h2>{{ __('Salas.Bienvenida')}}</h2>
        <!-- Solo tiene el logo con CSS -->
        <a class="welcome_logo" href="http://unlu.edu.ar" target="_blank"></a>
    </section>

    <!-- Botones Empezar partidas -->
    <section id="welcome_botones">
        <h2>{{ __('Salas.Botones')}}</h2>
        <button type="button" class="boton" id="boton_crear">{{ __('Salas.Crear') }}</button>
        <button type="button" class="boton" id="boton_buscador">{{ __('Salas.Lista') }}</button>
    </section>

    <!-- Boton de tutorial -->
    <section id="welcome_comoJugar">
        <h2>{{ __('Salas.Reglamento')}}</h2>
        <button type="button" class="boton" id="boton_reglamento">{{ __('Salas.ComoJugar') }}</button>
    </section>

    <section id="welcome_config">
        <h2>{{ __('Salas.Configuracion')}}</h2>
        <span class="video_icon" title="{{ __('Salas.Video_title')}}" id="video_icon"></span>
        <label id="video_label" for="video_icon">{{ __('Salas.Video')}}</label>
        <span class="sound_icon" title="{{ __('Salas.Audio_title')}}" id="sound_icon"></span>
        <label id="sound_label" for="sound_icon">{{ __('Salas.Sonido')}}</label>
    </section>

    <section id="pantalla-inactiva" class="ocultar">
        {{-- Este section es la pantalla en negro que aparece al abrir un pop-up --}}
    </section>

@endsection


@section('componentes')

    <!-- llamamos a la sub-vista del pop-up de buscador de salas -->
    <x-buscador-salas />
    <x-creador-salas />
    <x-reglamento-juego />
    <x-configuracion-sonido />
    @if ($errors->any())
        <x-error-validacion/>
    @endif

@endsection
