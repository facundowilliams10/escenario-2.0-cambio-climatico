@extends('layout')

@section('nombre_y_estilos')
    <title>{{ __('Inicio.TituloPagina') }}</title>
    <link rel="stylesheet" href="/css/app.css">
@endsection

@section('cuerpo_principal')
    <h1 class="h-video" >{{ __('Inicio.VideoExplicativo') }}</h1>
    <video controls autoplay muted>
        <source src="videos/cambio-climatico.mp4" type="video/mp4">
            {{ __('Inicio.ErrorVideo') }}
    </video>
    <a href="salas" id="skip">{{ __('Inicio.SaltarIntro') }}</a>
    <x-flash />
@endsection
