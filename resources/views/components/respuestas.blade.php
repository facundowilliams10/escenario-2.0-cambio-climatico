<section class="section-respuestas ocultar">
    <h3 id="enunciado"></h3>
    <form id="form-respuestas">
        @csrf
        <input id="boton_enviar_respuestas" type="button" value={{ __('Gameplay.Enviar') }}>
    </form>
</section>
