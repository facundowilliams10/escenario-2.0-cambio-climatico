{{-- Componente ---> Pop-up para buscar salas --}}

<section class="section-Pop-up buscadorSalas pop_up_hid">
    <h2>{{ __('Salas.Unirse') }}</h2>
    <form action="/unirseSala" method="post" class="form_listaSalas" name="fomulario_Buscar">
        @csrf
        <input type="hidden" id="sala_seleccionada" name="id_sala" value="">
        <fieldset class="salas">
            <ul id="lista_Salas">
                <li id="salas_no_disponibles">{{ __('Salas.NoDisponibles') }}</li>
            </ul>
        </fieldset>
        <fieldset class="botonesSalas">
            <button type="button" class="boton" id="boton_cerrar_buscador">{{ __('Salas.Cerrar') }}</button>
            <input type="submit" value="{{ __('Salas.Unirse') }}" class="boton">

        </fieldset>
    </form>
</section>
