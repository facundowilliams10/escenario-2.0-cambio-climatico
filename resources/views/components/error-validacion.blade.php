<section id="error_validacion" class="section-Pop-up">
    <h2>{{__("ErroresFormulario.Texto")}}</h2>
    <ul id="lista_errores">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
    <button id="boton_cerrar_errors" class="boton">{{__("ErroresFormulario.Cerrar")}}</button>
</section>