{{-- Componente ---> Pop-up para crear salas --}}
<section class="section-Pop-up creadorSalas pop_up_hid">

    <h2>{{__('CreadorSalas.Crear')}}</h2>

    <form action="{{ route('salas.create') }}" method="POST" class="form_crearSala" name="formulario_crear">
        @csrf
        <label for="crear_Name" class="label_crear_Name">{{__('CreadorSalas.Nombre')}}</label>
        <input type="text" name="sala_nombre" id="crear_Name" minlength="5" value="{{__('CreadorSalas.Ejemplo')}}">

        <label for="crear_TiempoJuego" class="label_radio_TJ">{{__('CreadorSalas.Tiempo')}}</label>
        <input type="number" name="sala_tiempoJuego" id="crear_TiempoJuego" min="10" max="60" value=30>
        <p id="text_tiempo">{{__('CreadorSalas.TiempoMax')}}</p>

        <label for="crear_NumJugadores" class="label_crear_NJ">{{__('CreadorSalas.Jugadores')}}</label>
        <input type="number" name="sala_numJugadores" id="crear_NumJugadores" min="1" max="40" value=2>
        <p id="text_numJug">{{__('CreadorSalas.JugadoresMax')}}</p>

        <label class="label_radio_DJ">{{__('CreadorSalas.Dificultad')}}</label>
        <fieldset class="radio_DJ">
            <input type="radio" name="sala_dificultad" id="radio_D_1" value="Facil" checked>
            <label for="radio_D_1">{{__('CreadorSalas.Facil')}}</label>
            <input type="radio" name="sala_dificultad" id="radio_D_2" value="Media">
            <label for="radio_D_2">{{__('CreadorSalas.Media')}}</label>
            <input type="radio" name="sala_dificultad" id="radio_D_3" value="Dificil">
            <label for="radio_D_3">{{__('CreadorSalas.Dificil')}}</label>
            <a id="dificultad_ayuda">info dificultad</a>
        </fieldset>

        <fieldset class="botonesCrear">
            <button type="button" class="boton" id="boton_cerrar_creador">{{ __('Salas.Cancelar') }}</button>
            <input type="submit" value="{{ __('Salas.Crear') }}" class="boton">
        </fieldset>

    </form>
</section>
