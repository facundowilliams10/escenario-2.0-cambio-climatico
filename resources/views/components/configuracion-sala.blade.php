{{-- Componente ---> Pop-up para configurar la sala creada --}}
<section class="config_sala pop_up_hid">

    <h2>{{ __('SalaCreada.Config') }}</h2>

    <label for="config_TiempoJuego" class="label_crear_tiempo"> {{ __('CreadorSalas.Tiempo') }}</label>
    <input type="number" id="config_TiempoJuego" placeholder="20" min="10" max="60">
    <p id="text_tiempo">{{ __('CreadorSalas.TiempoMax') }}</p>

    <label for="config_NumJugadores" class="label_crear_numJug"> {{ __('CreadorSalas.Jugadores') }}</label>
    <input type="number" id="config_NumJugadores" placeholder="8" min="2" max="40">
    <p id="text_numJug">{{ __('CreadorSalas.JugadoresMax') }}</p>

    <label class="label_radio_DJ"> {{ __('CreadorSalas.Dificultad') }}</label>
    <fieldset class="radio_DJ">
        <input type="radio" name="dificultad" id="radio_D_1" value="Facil">
        <label for="radio_D_1" id="label_D_1">{{ __('CreadorSalas.Facil') }}</label>
        <input type="radio" name="dificultad" id="radio_D_2" value="Media">
        <label for="radio_D_2" id="label_D_2">{{ __('CreadorSalas.Media') }}</label>
        <input type="radio" name="dificultad" id="radio_D_3" value="Dificil">
        <label for="radio_D_3" id="label_D_3">{{ __('CreadorSalas.Dificil') }}</label>
        {{--<a id="dificultad_ayuda">info dificultad</a>--}}
    </fieldset>

    <fieldset class="botonesCrear">
        <button id="config_cancelar" type="button" class="boton">{{ __('CreadorSalas.Cancelar') }}</button>
        <button id="config_guardar" type="button" class="boton">{{ __('CreadorSalas.Guardar') }}</button>
    </fieldset>

</section>
