@if (session()->has('success'))
    {{-- Si se usara solamente clase se da el siguiente problema: .GetElementsByClassname devuelve una colección, 
y al iterar la colección ejecutando .setTimeOut() da error porque es una funcion async. Queda pendiente la solucion. --}}
    <div id="mensajeExito" class="mensajeAlUsuario abajo">
        <p>{{ session('success') }}</p>
    </div>
@endif
<script>
    desaparecerFlash();

    function desaparecerFlash() {
        if (document.getElementById("mensajeExito"))
            setTimeout(function() {
                document.getElementById("mensajeExito").style.display = "none";
            }, 3000);
    }
</script>
