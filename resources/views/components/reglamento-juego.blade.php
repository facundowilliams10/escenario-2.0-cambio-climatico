{{-- Reglamento del Juego - Aqui se ponen todas las reglas, situaciones, etc --}}

<section class="section-Pop-up pop_up_hid reglamentoJuego">

    <span class="welcome_logo" id="logo_reglamento"></span>

    <h2>{{ __('Salas.ReglamentoJuego') }}</h2>

    <section class="reglamento_texto">
        <h2>{{ __('Reglamento.Instrucciones') }}</h2>
        <h3>{{ __('Reglamento.Equipos') }}</h3>
        <ul>
            <li>{{ __('Reglamento.EquiposTexto') }}</li>
        </ul>
        <h3>{{ __('Reglamento.CampoDeJuego') }}</h3>
        <ul>
            <li>{{ __('Reglamento.CampoDeJuegoTexto1') }}</li>
            <li>{{ __('Reglamento.CampoDeJuegoTexto2') }}</li>
            <li>{{ __('Reglamento.CampoDeJuegoTexto3') }}</li>
            <li>{{ __('Reglamento.CampoDeJuegoTexto4') }}</li>
            <li>{{ __('Reglamento.CampoDeJuegoTexto5') }}</li>
            <li>{{ __('Reglamento.CampoDeJuegoTexto6') }}</li>
        </ul>
        <h3>{{ __('Reglamento.DesarrolloDelJuego') }}</h3>
        <ul>
            <li>{{ __('Reglamento.DesarrolloDelJuegoTexto') }}</li>
        </ul>
        <h3>{{ __('Reglamento.Preguntas') }}</h3>
        <ul>
            <li>{{ __('Reglamento.PreguntasTexto1') }}</li>
            <li>{{ __('Reglamento.PreguntasTexto2') }}</li>
        </ul>
        <h3>{{ __('Reglamento.Acciones') }}</h3>
        <ul>
            <li>{{ __('Reglamento.AccionesTexto1') }}</li>
            <li>{{ __('Reglamento.AccionesTexto2') }}</li>    
        </ul>
        <h3>{{ __('Reglamento.Finalizacion') }}</h3>
        <h4>{{ __('Reglamento.FinalizacionTexto1') }}</h4>
        <ol>
            <li>{{ __('Reglamento.FinalizacionTexto2') }}</li>
            <li>{{ __('Reglamento.FinalizacionTexto3') }}</li>
            <li>{{ __('Reglamento.FinalizacionTexto4') }}</li>
        </ol>
    </section>

    <section id="botones_reglamento">
        <h2>{{ __('Reglamento.Botones') }}</h2>
        <button class="boton" id="boton_cerrar_reglamento">{{ __('Salas.Cerrar') }}</button>
    </section>

</section>