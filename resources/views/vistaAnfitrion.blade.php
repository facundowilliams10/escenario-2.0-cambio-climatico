@extends('layout')

@section('nombre_y_estilos')
    <title>{{ __('Anfitrion.TituloPagina') }}_{{ $sala->nombre }}</title>
    <script src="js/vistaAnfitrion.js"></script>
    <script src="js/paw.js"></script>
    <link rel="stylesheet" href="/css/vistaAnfitrion.css">
    <script>
        var tiempoJuego = [{!! $sala->tiempoJuego !!}, 0];
    </script>
@endsection


@section('cuerpo_principal')
    <section id="headers">
        <p id="nombre_sala" class="text">{{ __('Anfitrion.Sala') }} {{ $sala->nombre }}</p>
        <p id="nombre_usuario" class="text">{{ __('Anfitrion.Usuario') }} {{ $sala->anfitrion->name }}</p>
        <p id="minutos" class="text">{{ __('Anfitrion.TiempoRestante') }} <span id="temporizador">{{ $sala->tiempoJuego }}:00<span> </p>
        <p class="text">{{ __('Anfitrion.Etapa') }} <span id="etapa_actual"></span> </p>
    </section>

    <section id="info_equipos">
        <section id="equipo1_info" class="sect_info">
            {{-- Termometro --}}
            <svg width="90" height="150" viewBox="15 0 180 300" version="1.1" id="termometro_local" class="termometro"
                xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
                <defs id="defs2" />
                <g id="layer1">
                    <ellipse
                        style="opacity:0.923077;fill:#E19866;fill-opacity:1;stroke:#000000;stroke-width:8.638;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;paint-order:markers fill stroke"
                        id="path846" cx="104.59254" cy="222.5715" rx="39.623802" ry="40.873196" />
                    <rect
                        style="opacity:0.923077;fill:none;stroke:#000000;stroke-width:8.63801;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;paint-order:stroke fill markers;stroke-linecap:butt"
                        id="rect1004" width="39.266827" height="150.99879" x="84.959137" y="30.342548" />
                    <rect
                        style="opacity:0.923077;fill:#E19866;fill-opacity:1;stroke:none;stroke-width:8.33705;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                        id="medida_temp_local1" width="24.274038" height="0" x="92.812492" y="38.319386"
                        transform="scale(1,-1) translate(0,-215)" />
                </g>
            </svg>
            {{-- numero equipo --}}
            <p class="titulo">{{ __('Anfitrion.Equipo1') }}</p>
            {{-- grados --}}
            <p class="grados">ºC <span id="gr_equipo1"></span></p>
        </section>

        <section id="global_info" class="sect_info">
            {{-- Titulo --}}
            <p class="titulo">{{ __('Anfitrion.TempGlobal') }}</p>
            {{-- Termometro --}}
            <svg width="90" height="150" viewBox="15 0 180 300" version="1.1" id="termometro_local" class="termometro"
                xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
                <defs id="defs2" />
                <g id="layer1">
                    <ellipse
                        style="opacity:0.923077;fill:#FF0000;fill-opacity:1;stroke:#000000;stroke-width:8.638;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;paint-order:markers fill stroke"
                        id="path846" cx="104.59254" cy="222.5715" rx="39.623802" ry="40.873196" />
                    <rect
                        style="opacity:0.923077;fill:none;stroke:#000000;stroke-width:8.63801;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;paint-order:stroke fill markers;stroke-linecap:butt"
                        id="rect1004" width="39.266827" height="150.99879" x="84.959137" y="30.342548" />
                    <rect
                        style="opacity:0.923077;fill:#FF0000;fill-opacity:1;stroke:none;stroke-width:8.33705;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                        id="medida_temp_global" width="24.274038" height="0" x="92.812492" y="38.319386"
                        transform="scale(1,-1) translate(0,-215)" />
                </g>
            </svg>
            {{-- grados --}}
            <p class="grados">ºC <span id="gr_global"></span></p>
        </section>

        <section id="equipo2_info" class="sect_info">
            {{-- Termometro --}}
            <svg width="90" height="150" viewBox="15 0 180 300" version="1.1" id="termometro_local" class="termometro"
                xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
                <defs id="defs2" />
                <g id="layer1">
                    <ellipse
                        style="opacity:0.923077;fill:#E19866;fill-opacity:1;stroke:#000000;stroke-width:8.638;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;paint-order:markers fill stroke"
                        id="path846" cx="104.59254" cy="222.5715" rx="39.623802" ry="40.873196" />
                    <rect
                        style="opacity:0.923077;fill:none;stroke:#000000;stroke-width:8.63801;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;paint-order:stroke fill markers;stroke-linecap:butt"
                        id="rect1004" width="39.266827" height="150.99879" x="84.959137" y="30.342548" />
                    <rect
                        style="opacity:0.923077;fill:#E19866;fill-opacity:1;stroke:none;stroke-width:8.33705;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                        id="medida_temp_local2" width="24.274038" height="0" x="92.812492" y="38.319386"
                        transform="scale(1,-1) translate(0,-215)" />
                </g>
            </svg>
            {{-- numero equipo --}}
            <p class="titulo">{{ __('Anfitrion.Equipo2') }}</p>
            {{-- grados --}}
            <p class="grados">ºC <span id="gr_equipo2"></span></p>
        </section>
    </section>

    <button id="carga_pregunta" class="boton">{{ __('Anfitrion.CargarPreguntaStop') }}</button>
    <button id="finalizar_partida" class="boton">{{ __('Anfitrion.Finalizar') }}</button>

@endsection


@section('componentes')
    <x-carga-pregunta-stop />
@endsection
