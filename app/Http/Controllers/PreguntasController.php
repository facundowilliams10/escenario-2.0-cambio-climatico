<?php

namespace App\Http\Controllers;

use App\Models\Pregunta;
use App\Models\Respuesta;
use App\Models\Sala;
use App\Models\AccionRealizada;
use App\Models\User;
use App\Models\Stop;
use App\Models\StopRespuesta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;

class PreguntasController extends Controller
{
    public function create()
    {
        $user = User::find(auth()->user()->id);
        if ($user->email == "famawilliams@gmail.com") {
            return view('preguntas.create');
        } else {
            return redirect()->route("salas");
        }
    }

    public function getPregunta(Request $request, $elemento_asociado)
    {
        return Pregunta::getPregunta(auth()->user()->id, $elemento_asociado, $request);
    }

    public function responder(Request $request)
    {
        return Pregunta::responder($request, auth()->user()->id);
    }

    public function cargarPreguntaStop(Request $request)
    {
        // $request->validate([
        //     'pregunta' => 'required|string|min:10|max:100',
        //     'justificacion' => 'required|string|min:20|max:255',
        //     'minutos' => 'required|min:1|max:3',
        //     'grados' => 'required'
        // ]);
        return Stop::cargarPreguntaStop($request);
    }

    public function getStop($id_sala)
    {
        return Stop::getStop($id_sala);
    }

    public function responder_stop(Request $request){
        return Stop::responder_stop($request);
    }

    public function store(Request $request)
    { 
        $request->validate([
            'enunciado' => 'required',
            'dificultad' => 'required',
            'elemento_asociado' => 'required',
            'justificacion' => 'required',
            'respuesta1' => 'required',
            'grados1' => 'required',
            'respuesta2' => 'required',
            'grados2' => 'required',
        ]);

        Pregunta::crear($request);
        return redirect()->route("salas");
    }

    /* Delete This */
    public function migration()
    {
        $preguntas =  Pregunta::all();
        $migrationText = "";
        foreach ($preguntas as $pregunta) {
            $migrationText = $migrationText . '$request = [
                "idioma" => "es",
                "elemento_asociado" =>"'. $pregunta->elemento_asociado.'",
                "enunciado" => "'.$pregunta->enunciado.'",
                "justificacion" => "'.$pregunta->justificacion.'",
                "etapa" => "'.$pregunta->etapa.'",
                "dificultad" => "'.$pregunta->dificultad.'",';
            $respuestas = $pregunta->respuestas;
            $index = 1;
            foreach ($respuestas as $respuesta) {
                $migrationText = $migrationText . '"respuesta'.$index.'" => "'.$respuesta->texto.'",
                    "grados'.$index.'" => '.$respuesta->grados.',';
                $index = $index + 1;
            }
            $migrationText = $migrationText . '];' . 'Pregunta::crear($request);';
        }

        return $migrationText;
    }

    public function databaseDump()
    {
        $preguntas =  Pregunta::all();
        $databaseDumpText = "";
        foreach ($preguntas as $pregunta) {
            $databaseDumpText = $databaseDumpText . '$pregunta = Pregunta::create([
            "elemento_asociado" => "'.$pregunta->elemento_asociado.'",
            "enunciado" => "'.$pregunta->enunciado.'",
            "dificultad" => "'.$pregunta->dificultad.'",
            "etapa" => "'.$pregunta->etapa.'",
            "justificacion" => "'.$pregunta->justificacion.'",
            "language" => "'.$pregunta->language.'"
        ]);';
            $respuestas = $pregunta->respuestas;

            $menoresGrados = 1000000;
            $texto = "";
            foreach ($respuestas as $respuesta) {
                if ($respuesta->grados < $menoresGrados){
                    $texto = $respuesta->texto;
                    $menoresGrados = $respuesta->grados;
                }
            }

            foreach ($respuestas as $respuesta) {
                $databaseDumpText = $databaseDumpText . 'Respuesta::create([
                        "texto" => "'.$respuesta->texto.'",
                        "pregunta_id" => $pregunta->id,';
                    if ($respuesta->texto == $texto) {
                        $databaseDumpText = $databaseDumpText .'"isOptima" => 1,
                        "grados" => "$gradosOptima",
                        ]);';
                    } else {
                        $databaseDumpText = $databaseDumpText .'"isOptima" => 0,
                        "grados" => "$gradosIncorrecta",
                        ]);';
                    }
            }
        }

        return $databaseDumpText;
    }
}
