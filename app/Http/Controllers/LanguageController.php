<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Session\Session as ContractsSessionSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Session\Session as SessionSession;

class LanguageController extends Controller
{
    /**
     * Change language
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function changeLanguage(Request $request)
    {
        $validated = $request->validate([
            'language' => 'required',
        ]);

        session()->put('language', $request->language);

        return redirect()->back();
    }
}
