<?php

namespace App\Http\Controllers;

use App\Models\Equipo;
use Illuminate\Http\Request;

class EquiposController extends Controller
{
    public function moveJugador(Request $request)
    {
        return Equipo::moverJugador($request);
    }

    public function kickearJugador(Request $request)
    {
        return Equipo::kickearJugador($request);
    }
}
