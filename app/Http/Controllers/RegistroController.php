<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class RegistroController extends Controller
{
    public function create()
    {
        return view('registro.crear');
    }

    public function store()
    {

        // //Se valida y guarda el usuario
        $atributos = request()->validate([
            'name' => 'required|max:255|min:3|unique:users,name',
            'password' => 'required|max:255|min:6',
            'email' => 'required|email|max:255|unique:users,email'
        ]);

        $usuario = User::create($atributos);

        //Se loguea al usuario

        Auth::login($usuario);
        // session()->flash('success', '¡Tu cuenta ha sido creada con éxito!');

        return redirect('/tutorial')->with('success', '¡Tu cuenta ha sido creada con éxito!');
    }
}
