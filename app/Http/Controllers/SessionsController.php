<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class SessionsController extends Controller
{
    //desconecta al usuario (logout)
    public function destroy()
    {
        Auth::logout();

        return redirect("/")->with("Exito", "¡Chau!");
    }

    public function create()
    {
        return view("sessions.create");
    }

    public function store()
    {

        $credenciales = request()->validate([
            'email' => 'required|email',
            'password' => 'required '
        ]);
        if (Auth::attempt($credenciales)) {
            session()->regenerate(); // para evitar session fixation
            return redirect('inicio')->with("Exito", "¡Iniciaste sesión !");
        }
        //pero si falla la autenticacion:
        throw ValidationException::withMessages(
            ['email' => 'correo y contraseña ingresados son incorrectos']
        );
    }
}
