<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //Si no está logeado el admin devuelve 403
        if (auth()->user()?->email !== 'famawilliams@gmail.com')
        abort(Response::HTTP_FORBIDDEN);
    
        return $next($request);
    }
}
