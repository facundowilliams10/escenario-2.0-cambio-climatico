<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Exception;

class Stop extends Model
{
    use HasFactory;
    protected $fillable = ['enunciado', 'estado', 'justificacion', 'minutos', 'grados'];

    public function respuestas()
    {
        return $this->hasMany(StopRespuesta::class);
    }

    public function salas()
    {
        return $this->belongsToMany(Sala::class)
            ->withPivot('estado');//Atributo de la relación N a N entre Stop y Salas
    }
    //EJEMPLO PARA RECUPERAR LAS INSTANCIAS D ELA RELACION "N A N"
    //$stop = Stop::find(1);
    //foreach ($stop->salas as $sala) {
    //
    // }

    public static function crear(
        $atributos
    ) {
        // TODO cambiar para que sea con cantidad de opciones cualquiera (no creo que sean más de 4)
        try {
            $nuevo_stop = self::create([
                'enunciado'       => $atributos->pregunta,
                'justificacion'  => $atributos->justificacion,
                'minutos'  => $atributos->minutos,
                'grados'   => $atributos->grados,
            ]);
            return $nuevo_stop;
        } catch (Exception $e) {
            return null;
        }
    }

    public static function asignarPregunta($id_sala, $id_stop)
    {
        $stop = self::find($id_stop);
        $stop->salas()->attach($id_sala, ['estado' => 'abierto']);
    }

    public static function cargarPreguntaStop($request)
    {
        $array = json_decode($request->getContent());
        $id_sala = $request->cookie('cook_id_sala');
        // Cerrar STOP actual
        self::cerrarStopActual($id_sala);
        // Guarda pregunta relacionada con la sala. Crear en tabla stops la pregunta, y en stop_respuestas las respuestas
        $stop = self::crear($array);
        // Cargar las opciones
        StopRespuesta::asignarOpciones($stop->id, $array);
        // Relacionar la pregunta con id_sala (relacion n a n)
        self::asignarPregunta($id_sala, $stop->id);
        // Cambiar estado de la partida a STOP
        Sala::stopSala($id_sala);
    }

    public static function getStop($id_sala)
    {
        try {
            // Recuperar stop
            $stop = Sala::getStop($id_sala);
            return json_encode([
            "pregunta" => $stop->enunciado,
            "justificacion" => $stop->justificacion,
            "minutos" => $stop->minutos,
            "respuestas" => $stop->respuestas,
            ]);
        } catch (Exception $e) {
            return json_encode([
                "pregunta" => "",
                "justificacion" => "",
                "minutos" => "",
                "respuestas" => "",
                ]);
        }
    }

    public static function responder_stop($request)
    {
        try {
            $json_data = json_decode($request->getContent());

            if ($json_data->respuesta == "") { // El usuario no contestó nada
            } else { // El usuario llegó a elegir
                // obtener sala
                $sala = Sala::find($request->cookie('cook_id_sala'));
                // obtener stop
                $stop = Sala::getStop($sala->id);
                // obtener respuesta seleccionada
                $respuesta = $stop->respuestas->where('texto', $json_data->respuesta)->first();
                // crear nuevo voto_user
                $respuesta->votos()->attach(auth()->user()->id);
                $respuesta->save();
            }
            return json_encode("respondido exitosamente");
            
        } catch (Exception $e) {
            return json_encode(false);
        }
    }

    public static function cerrarStopActual($sala_id)
    {
        $sala = Sala::find($sala_id);
        $sala_stop = Sala::getStop($sala_id);
        if(isset($sala_stop)){
            $sala->stops()->updateExistingPivot($sala_stop->pivot->stop_id, [
                'estado' => 'cerrado'
            ]);
        }
    }

    // Devuelve el texto de la respuesta optima de la pregunta Stop
    public static function getRespuestaOptima($stop)
    {
        $respuestaOptima = $stop->respuestas->where("isOptima", 1)->first();
        return $respuestaOptima->texto;
    }
}
