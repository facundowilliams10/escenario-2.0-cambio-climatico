<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    use HasFactory;
    protected $table = "respuestas";

    protected $primaryKey = "id";

    protected $guarded = [];

    public function pregunta()
    {
        return $this->belongsTo(Pregunta::class);
    }

    public static function obtenerRespuestas($request){
        $result = [];
        for ($i = 1; $i <= 10; $i++) {
            try{
                if (!isset($request["respuesta".$i]))
                    break;
                $result[$i] = ["respuesta".$i, $request["respuesta".$i], $request["grados".$i]];
            } catch (Exception $e){
                break;
            }
        }
        return $result;
    }

    public static function crear($idPregunta, $respuestas, $idPreguntaTraducida, $respuestasTraducidas)
    {
        $respuestaOptima = self::obtenerRespuestaOptima($respuestas);
        
        for ($i = 1; $i <= count($respuestas); $i++) {
            $optima = 0;
            if ($respuestaOptima == $respuestas[$i][0])
                $optima = 1;

            Self::create([
                "texto" => $respuestas[$i][1],
                "grados" => $respuestas[$i][2],
                "isOptima" => $optima,
                "pregunta_id" => $idPregunta,
            ]);

            Self::create([
                "texto" => $respuestasTraducidas[$i+1]["translatedText"],
                "grados" => $respuestas[$i][2],
                "isOptima" => $optima,
                "pregunta_id" => $idPreguntaTraducida,
            ]);
        }
    }

    public static function obtenerRespuestaOptima($respuestas)
    {
        $respuestaOptima = "";
        $menoresGrados = 1000;

        forEach($respuestas as $respuesta){
            if ($respuesta[2] < $menoresGrados){
                $respuestaOptima = $respuesta[0];
                $menoresGrados = $respuesta[2];
            }
        }

        return $respuestaOptima;
    }
}
