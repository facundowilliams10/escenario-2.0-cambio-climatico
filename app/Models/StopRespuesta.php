<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Exception;

class StopRespuesta extends Model
{
    use HasFactory;
    protected $fillable = ['texto', 'isOptima', 'stop_id'];

    public function Stop()
    {
        return $this->belongsTo(Stop::class);
    }

    public function votos()
    {
        return $this->belongsToMany(User::class, "votos_users", "stop_respuesta_id", "user_id");
    }

    public static function crear(
        $texto,
        $isOptima,
        $stop_id
    ) {
        try {
            $nueva_stopRespuesta = self::create([
                'texto'       => $texto,
                'isOptima'  => $isOptima,
                'votos'  => 0,
                'stop_id'   => $stop_id,
            ]);
            return $nueva_stopRespuesta;
        } catch (Exception $e) {
            return Null;
        }
    }

    public static function asignarOpciones($id_stop, $atributos)
    {
        try {
            foreach($atributos->opciones as $opcion){
                self::crear($opcion[0], $opcion[1], $id_stop);
            }
        } catch (Exception $e) {
            return Null;
        }
    }

}
