<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    //use HasFactory;
    //Ejemplo: Un profesor tiene muchos permisos(cargar preguntas, crear salas...)
    public function permisos()
    {
        return $this->belongsToMany(Permiso::class)->withTimestamps();
    }

    public function permitir($permiso)
    {
        $this->permisos()->sync($permiso, false); //Con false le indicamos que no elimine registros
        //Con sync le indicamos que si yua existía el registro lo reemplace 
        //(es para evitar que de error porque ya existía un registro con esda PK)
    }
}
