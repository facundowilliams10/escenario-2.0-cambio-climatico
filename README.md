# Instrucciones para ejecutar el proyecto :zap:

## Requisitos

- Docker
    https://docs.docker.com/get-docker/ 
- Docker Compose (Windows ya lo trae instalado)
    https://docs.docker.com/compose/install/
- PHP 8.1.0
    https://www.php.net/manual/es/install.php 
- Composer 
    -https://getcomposer.org/download/ 

## Pasos para ejecutar y probar el proyecto

1. Instalar las dependencias del proyecto

    En una terminal, dentro del directorio, escribir

    ```php
    composer update
    ```
    ```php
    compose install
    ```

2. Configurar arhivo .env

    - Renombrar el archivo example.env a .env

    - Ejecutar el comando:
    ```php
    php artisan key:generate
    ```

3. Ingresar a phpmyadmin y crear la bd

    - Codificacion: uft8_general_ci
    - Nombre: escenario2

4. En una terminal, ubicarse en la carpeta del proyecto y ejecutar:

    ```php
    php artisan serve
    ```

    _Si falla el comando php artisan serve actualizar Compose_
    ```php
    composer update
    ```

5. Cargar registros de prueba de forma rápida en la Base de Datos y configurar el usuario admin

    ```php
    php artisan migrate:fresh --seed
    ```

5. Ingresar al siguiente link

    http://127.0.0.1:8000/


## Usuarios pre-cargadas

### Usuario tipo profesor con permisos para cargar preguntas y respuestas

    - Email: famawilliams@gmail.com
    - Contraseña: FacuSebaAngel2021

### Usuarios de tipo alumno

    - Email: qwe@gmail.com
    - Contraseña: qweqwe

    - Email: asd
    - Contraseña: asdasd

## Cheatsheet para administrar la Base de Datos

- instalar mysql
- Ejecutar 
```php
mysql -u root -p
```
- Ejecutar 
```php
create database escenario2;
```php
- Ejecutar en el directorio del proyecto
```php
php artisan migrate
```
- Dentro de la BD ejecutar 
```php
show tables;
```
- Para validar la migración debería ver algo como:
```php
mysql> show tables;
+----------------------+
| Tables_in_escenario2 |
+----------------------+
| failed_jobs          |
| migrations           |
| password_resets      |
| users                |
+----------------------+
4 rows in set (0.01 sec)
```
Para borrar todas las tablas y volver a migrarlas
```php
php artisan migrate:fresh
```

Para además cargar registros de prueba
```php
php artisan migrate:fresh --seed
```

#### Cheatsheet para mapear un registro de la bd a un objeto Eloquent
```php
php artisan tinker
```
- Ejemplo:
```php
$user = new User;
-$user->name = 'Facundo'
-$user->email = 'facundowilliams10@gmail.com'
...
```
- Para encriptar el password: 
```php
$user->password = bcrypt('unlu')
```
- Para guardar: 
```php
$user->save();
```
- Para obtener solo el valor de una columna: 
```php
$users->pluck('name');
```
-Para hacer rollback(deshace las migraciones del ultimo batch en la BD): ´php artisan migrate:rollback´ 
- Link a la API completa con todos los métodos:
    https://laravel.com/api/5.8/Illuminate/Database/Eloquent.html 

#### Crear Modelos con Eloquent (ejemplo para la entidad "pregunta")
- Crear migración ¡Usar nombre en plural!
```php
php artisan make:migration create_preguntas_table
``` 
- Se agregan los campos en database/migrations/...create_pregunta_table.php
- Crear modelo ¡Usar nombre en singular y primera letra en mayuscula!
```php
php artisan make:model Pregunta 
```
- Realizar la migración
```php
php artisan migrate
```

##### Atajo (crea el modelo y migra)
```php
php artisan make:model Pregunta -m
```
#### Editar registros en la BD (ejemplo)

##### Primera forma
```php
php artisan tinker
$pregunta = App\Models\Pregunta::first();
$pregunta->enunciado = '¿' . $pregunta->enunciado;
$pregunta->save();
```
##### Forma alternativa
```php
php artisan tinker´
$pregunta = App\Models\Pregunta::first(); 
pregunta->update([enunciado => 'pruebita'])
```

#### Testear preguntas
-Dar de alta un registro en la tabla preguntas
-Ejemplo: Ir a http://localhost:8000/preguntas/1

### Otros
-Para mantener compatibilidad con las funciones flecha se necesita la ultima version de php
-Para limpiar la caché de Laravel ejecutar 
```php
$ php artisan optimize:clear
```
-Para ver los comandos disponibles ejecutar 
```php
php artisan
```
-Para crear controlers 
```php
php artisan make:controller <controller-name> --plain
```

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[OP.GG](https://op.gg)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
