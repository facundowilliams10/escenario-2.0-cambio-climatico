class popupRespuestas {
    constructor(pContenedor) {
        //conseguimos el nodo
        let contenedor = pContenedor.tagName
            ? pContenedor
            : document.querySelector(pContenedor);

        if (contenedor) {
            contenedor.classList.add("PAW-Menu");
            contenedor.classList.add("PAW-MenuCerrado");

            let css = PAW.nuevoElemento("link", "", {
                rel: "stylesheet",
                href: "js/components/styles/ejemplo.pawmenu.css",
            });
            document.head.appendChild(css);

            // Armar Boton
            let boton = PAW.nuevoElemento("button", "", {
                class: "PAW-MenuAbrir",
            });

            boton.addEventListener("click", (event) => {
                if (event.target.classList.contains("PAW-MenuAbrir")) {
                    event.target.classList.add("PAW-MenuCerrar");
                    event.target.classList.remove("PAW-MenuAbrir");
                    contenedor.classList.add("PAW-MenuAbierto");
                    contenedor.classList.remove("PAW-MenuCerrado");
                } else {
                    event.target.classList.add("PAW-MenuAbrir");
                    event.target.classList.remove("PAW-MenuCerrar");
                    contenedor.classList.add("PAW-MenuCerrado");
                    contenedor.classList.remove("PAW-MenuAbierto");
                }
            });

            // Insertar boton en el NAV
            contenedor.prepend(boton);
        } else {
            console.error("Elemento HTML para generar el MENU no encontrado");
        }
    }
}
