class VistaAnfitrion {
    ref_juego;
    ref_tiempo;
    self;
    constructor() {
        document.addEventListener("DOMContentLoaded", () => {
            self = this;
            // Refresca la informacion relevante de los equipos.
            this.ref_juego = setInterval(this.refresh_juego, 2000);
            //Temporizador.
            this.ref_tiempo = setInterval(this.actualizarTemporizador, 1000);  

            // Boton para cargar una pregunta STOP
            document.querySelector("#carga_pregunta").addEventListener("click", () => {
                let ventana_STOP = document.querySelector(
                    "#cargaPreguntaSection"
                );
                ventana_STOP.classList.remove("hidden");
            });

            // Boton cancelar pregunta STOP
            document.querySelector("#cancelar_pregunta").addEventListener("click", () => {
                document
                    .querySelector("#cargaPreguntaSection")
                    .classList.add("hidden");
            });
            // Boton agregar opcion Stop
            document.querySelector('#boton_agregar_opcion').addEventListener('click', () => {
                let numeroOpcion = document.querySelectorAll('.opcion').length + 1;
                this.agregarOpcion(numeroOpcion);
            });
            // Boton lanzar pregunta
            document.querySelector("#lanzar_pregunta").addEventListener("click", () => {
                let pregunta = document.getElementById("input_pregunta").value;

                let opciones = this.getOpcionesStop();

                let justificacion = document.querySelector(
                    "#input_justificacion"
                ).value;

                let minutos = document.querySelector("#input_minutos").value;

                let grados = document.querySelector("#input_grados").value;

                this.fetchCargarStop(
                    pregunta, 
                    opciones, 
                    justificacion, 
                    minutos, 
                    grados
                );
                
            });
            // Boton agregar opcion Stop
            document.querySelector('#finalizar_partida').addEventListener('click', () => {
                this.finalizar_partida();
            });
        });
    }

    refresh_juego() {
        fetch("/refrescarJuego")
            .then((response) => response.json())
            .then((data) => {
                //update termometros.
                self.refrescarTermometros(
                    "#medida_temp_local1",
                    "#gr_equipo1",
                    data.tEquipo1
                );
                self.refrescarTermometros(
                    "#medida_temp_local2",
                    "#gr_equipo2",
                    data.tEquipo2
                );
                self.refrescarTermometros(
                    "#medida_temp_global",
                    "#gr_global",
                    data.tempGlobal
                );

                let etapa_text = document.getElementById("etapa_actual");
                etapa_text.innerHTML = data.etapa;
                self.refrescarVista(data.estado)
                //VistaAnfitrion.refrescarTermometros( '#medida_temp_global', data.tempGlobal );
            })
            .catch(function (error) {
                console.log(
                    "Hubo un problema con la petición Fetch:" + error.message
                );
            });
    }

    // Esta funciona redirecciona al usuario si finalizo la partida.
    refrescarVista(estado) {
        if ("finalizado" == estado)         
            window.location.href = "/obtenerVistaFinJuego";
    }

    refrescarTermometros(idTerm, idSpan, temp) {
        if ("" !== temp) {
            let term = document.querySelector(idTerm);
            term.classList.add("termo");
            term.setAttribute("height", temp);
            let gr_text = document.querySelector(idSpan);
            gr_text.textContent = temp;
        }
    }

    actualizarTemporizador() {
        let time = document.querySelector("#temporizador");
        let min = "";
        let seg = "";

        if ((tiempoJuego[0] == 0) & (tiempoJuego[1] == 0)) {
            clearInterval(this.ref_tiempo);
        } else if (tiempoJuego[0] === 0) {
            tiempoJuego[1] -= 1;
        } else {
            if (tiempoJuego[1] === 0) {
                tiempoJuego[1] = 59;
                tiempoJuego[0] -= 1;
            } else {
                tiempoJuego[1] -= 1;
            }
        }
        seg = tiempoJuego[1].toString();
        min = tiempoJuego[0].toString();

        if (seg.length === 1) {
            seg = "0" + seg;
        }

        if (min.length === 1) {
            min = "0" + min;
        }

        time.textContent = min + ":" + seg;
    }

    agregarOpcion(numeroOpcion)
    {
        let section_opciones = document.querySelector('#section_opciones');
        var palabraOpcion = section_opciones.querySelector("label").innerText.split(" ")[0];

        let label = PAW.nuevoElemento(
            'label', 
            palabraOpcion+' ' + (numeroOpcion) + ": ",
            {
                for: "input_opcion_" + (numeroOpcion)
            }
        );
        
        let inputText = PAW.nuevoElemento(
            'input',
            "",
            {
                type: "text",
                id: "input_opcion_" + numeroOpcion,
                name: "opcion" + numeroOpcion,
                class: "input_opcion"
            }
        );
        
        let radio = PAW.nuevoElemento(
            'input',
            "",
            {
                type: "radio",
                name: "isOptima",
                id: "radio_opcion_" + numeroOpcion,
                class: "radio_optima"
            }
        );

        let fieldset = PAW.nuevoElemento(
            "fieldset",
            "",
            {
                id: "opcion_" + numeroOpcion,
                class: "opcion"
            }
        );

        // span mis <span class="input_menos"></span>
        let span_minus = PAW.nuevoElemento(
            'span',
            "",
            {
                class: "input_menos"
            }
        );
        
        span_minus.addEventListener('click', ()=>{
            // quitar opcion
            section_opciones.removeChild(fieldset);
            // renombrar opciones
            
        });

        fieldset.appendChild(label);
        fieldset.appendChild(inputText);
        fieldset.appendChild(radio);
        fieldset.appendChild(span_minus);
        
        section_opciones.insertBefore(fieldset, section_opciones.querySelector("#section_agregar_opcion"));
    }

    getOpcionesStop()
    {
        let opciones = [];
        let arrayOps = document.querySelectorAll('.opcion');

        arrayOps.forEach( function(opcion, index) {
            let numeroOpcion = index + 1;
            let op = [];
            op[0] = opcion.querySelector("#input_opcion_" + numeroOpcion).value;
            op[1] = opcion.querySelector("#radio_opcion_" + numeroOpcion).checked;
            opciones[index] = op;
        });

        return opciones;
    }

    fetchCargarStop(pregunta, opciones, justificacion, minutos, grados)
    {
        let body = JSON.stringify({
            pregunta: pregunta,
            justificacion: justificacion,
            minutos: minutos,
            opciones: opciones,
            grados: grados,
        });

        let myRequest = PAW.armarRequest(
            "/cargarPreguntaStop",
            body,
            "csrf-token",
            "content"
        );

        fetch(myRequest)
        .then(response => {
            if(response.ok){
                console.log(response);
                return response.json();
            }
            return response.text().then(text => {throw new Error(text)})
        })
            .catch(function (error) {
                console.log(
                    "Hubo un problema con la petición Fetch:" + error
                );
            });
    }

    finalizar_partida() {
        var myRequest = PAW.armarRequest(
            "/finalizarSala",
            '{}',
            "csrf-token",
            "content"
        );

        fetch(myRequest)
            .then((response) => {
                if (response.ok) {
                    window.location.href = "/salas";
                }
            })
            .catch(function (error) {
                console.log(
                    "Hubo un problema con la petición Fetch:" + error.message
                );
            });
    }
}
    let interfazAnfi = new VistaAnfitrion();