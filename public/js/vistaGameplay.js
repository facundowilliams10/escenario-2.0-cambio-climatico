class vistaGameplay {
    ref_juego;
    ref_tiempo;
    ref_bienestar;
    self;
    id_sala;
    zoom;
    mostrar_stop;            /* booleano, para mostrar la pregunta Stop una vez */
    static mostrar_resultado_stop;  /* booleano, para mostrar justifiacion Stop una vez */
    static bienestar_actual;
    static ultimo_refresh;
    static tiempo_refresh;
    constructor() {
        // La variable id_sala, la cargo en la plantilla de blade.
        self = this;
        let ultimo_elemento_clickeado;

        self.tiempo_refresh = 5;

        self.ultimo_refresh = new Date().getTime()-100000; //Necesario
        // Refresca elementos del juego.
        this.ref_juego = setInterval(this.refresh_juego, 1000); 
        //Ese 1 seg no implica que el refresh se va a hacer cada 1 segundo
        //Temporizador.
        this.ref_tiempo = setInterval(this.actualizarTemporizador, 1000);

        this.ref_bienestar = setInterval(this.simularDecrementoBienestar, 1000);

        this.mostrar_stop = false;
        
        self.mostrar_resultado_stop = false;
        self.zoom = 100;
        
        let mensajeRotarPantalla =
        document.querySelector(".rotar-pantalla");

        //Si el dispositivo está en modo horizontal muestramos el mensaje
        if ( (matchMedia('all and (orientation:landscape)').matches) ) 
        mensajeRotarPantalla.classList.add("no-mostrar");

        //Si el dispositivo cambia la orientación mostramos el mensaje si no es horizontal
        window.addEventListener("orientationchange", function(event) {
            if ( event.target.screen.orientation.angle ==90 ){
                mensajeRotarPantalla.classList.add("no-mostrar");
            }
            else
                mensajeRotarPantalla.classList.remove("no-mostrar");
        });
    

        // Setear eventos para elementos del mapa.
        document.querySelectorAll(".elemento_mapa").forEach((element) =>
            element.addEventListener("click", () => {
                this.cargar_pregunta(element.id);
            })
        );

        document.querySelector("#boton_cerrar_correcion").addEventListener("click", () => {
            document
                .querySelector(".section-correcion")
                .classList.add("ocultar");
        });

        document.querySelector("#boton_enviar_respuestas").addEventListener("click", () => {
            let respuesta = this.obtener_respuesta_seleccionada(
                document.querySelector(".section-respuestas")
            );
            this.responderPregunta(respuesta, this.ultimo_elemento_clickeado);
        });

        document.querySelector("#boton_stop_respuestas").addEventListener("click", () => {
            let respuesta = this.obtener_respuesta_seleccionada(
                document.querySelector("#section-stop")
            );
            this.responderPreguntaStop(respuesta);
        });

        document.querySelector("#cerrar_resultado").addEventListener("click", ()=>{
            // Cierra la ventana de "esp"
            document.querySelector("#justificacion-stop").classList.add("ocultar");
        });

        // Comportamiento de zoom
        document.getElementById("zoom_in").addEventListener("click",function(e){
            if (self.zoom < 300){
                self.zoom+=20;
                self.resize();
            }
        });

        document.getElementById("zoom_out").addEventListener("click",function(e){
            if(self.zoom > 50){
                self.zoom-=20;
                self.resize();
            }
        });
    }

    mostrar_correcion(justificacion, is_correcta) {
        let section_correcion = document.querySelector(".section-correcion");
        section_correcion.classList.remove("ocultar");

        let correcion = section_correcion.querySelector("#correcion");

        correcion.innerHTML = justificacion;

        if (is_correcta) {
            section_correcion.classList.remove("respuesta-incorrecta");
            section_correcion.classList.add("respuesta-correcta");
        } else {
            correcion.innerHTML = justificacion;
            section_correcion.classList.remove("respuesta-correcta");
            section_correcion.classList.add("respuesta-incorrecta");
        }
    }

    cargar_pregunta(elemento_asociado) {
        this.ultimo_elemento_clickeado = elemento_asociado;
        fetch("/getPregunta/" + elemento_asociado)
            .then((response) => response.json())
            .then((data) => {
                if (data.valid_timestamp && data.valid_etapa) {
                    let section_respuestas = document.querySelector(
                        ".section-respuestas"
                    );
                    section_respuestas.classList.remove("ocultar");

                    let form_respuestas =
                        section_respuestas.querySelector("#form-respuestas");


                    form_respuestas
                        .querySelectorAll(".respuesta")
                        .forEach((child) => form_respuestas.removeChild(child));

                    form_respuestas
                        .querySelectorAll("span")
                        .forEach((child) => form_respuestas.removeChild(child));

                    form_respuestas
                        .querySelectorAll("label")
                        .forEach((child) => form_respuestas.removeChild(child));

                    document.querySelector("#enunciado").innerHTML =
                        data.pregunta;

                    let letter_number = 97;
                    data.respuestas.forEach((respuesta) =>
                        this.crear_input(respuesta, letter_number++)
                    );
                } else {
                    this.mostrar_correcion(data.justificacion, false);
                }

                self.efectuarRefreshJuego(data);
            })
            .catch(function (error) {
                console.log(
                    "Hubo un error con el fetch  >>>  " + error.message
                );
            });
    }

    crear_input(respuesta, letter_number) {
        let section_respuestas = document.querySelector(".section-respuestas");
        let form_respuestas =
            section_respuestas.querySelector("#form-respuestas");
        let input_enviar = form_respuestas.querySelector(
            "#boton_enviar_respuestas"
        );
        form_respuestas.insertBefore(
            PAW.nuevoElemento("input", "", {
                value: String.fromCharCode(letter_number),
                type: "radio",
                name: "respuesta",
                class: "respuesta",
            }),
            input_enviar
        );

        form_respuestas.insertBefore(
            PAW.nuevoElemento("label", respuesta.texto, {
                id: "respuesta_" + String.fromCharCode(letter_number),
            }),
            input_enviar
        );
    }

    obtener_respuesta_seleccionada(section) {
        let return_value = "";
        var respuestas = section.querySelectorAll(".respuesta");
        respuestas.forEach((respuesta) => {
            if (respuesta.checked) {
                var id_span = "respuesta_" + respuesta.value;
                var span = section.querySelector("#" + id_span);
                return_value = span.innerHTML;
            }
        });
        return return_value;
    }

    refresh_juego() {
        if (!self.refreshNecesario())
            return
        fetch("/refrescarJuego")
            .then((response) => response.json())
            .then((data) => {
                //console.log(data);
                //console.log(self.mostrar_resultado_stop)
                // self.ocultarVentanas();
                if (data.estado == "stop") {
                    // this.pararTimer();
                    self.cargarStop(data);
                }
                self.cargarResultadoStop(data);
                self.efectuarRefreshJuego(data);
            })
            .catch(function (error) {
                console.log(
                    "Hubo un problema con la petición Fetch:" + error
                );
            });
    }

    refreshNecesario(){
        var dif = new Date().getTime() - self.ultimo_refresh;

        var seconds = Math.round(dif / 1000);

        return (seconds >= self.tiempo_refresh);
    }

    efectuarRefreshJuego(data) {

        self.ultimo_refresh = new Date().getTime();

        self.refrescarVista(data.estado, data.etapa);

        self.refrescarTermometros("#medida_temp_local", data.tempLocal);

        self.refrescarTermometros("#medida_temp_global", data.tempGlobal);

        self.disponibilizar_elementos(data.elementos_disponibles);

        self.verificarTemporizador(data.tiempoJugando, data.tiempoJuegoSala);

        if(data.estado != "stop"){
            self.refrescarBienestar(data.bienestar);
        }

        //Solo muestra la pantalla gris de inactividad
        //  vistaGameplay.mostrarPantallaInactiva(data.usuario_activo);
    }

    quitar_disponibilidad_elementos_mapa() {
        var elementos_mapa = document.querySelectorAll(".elemento_mapa");
        elementos_mapa.forEach((element) =>
            element.classList.remove("elemento_mapa_disponible")
        );
    }

    disponibilizar_elementos(elementos_disponibles) {
        self.quitar_disponibilidad_elementos_mapa();

        elementos_disponibles.forEach((element) =>
            document
                .querySelector("#" + element.elemento_asociado)
                .classList.add("elemento_mapa_disponible")
        );
    }

    refrescarTermometros(idTerm, temp) {
        if ("" !== temp) {
            let term = document.querySelector(idTerm);
            term.classList.add("termo");
            term.setAttribute("height", temp);
        }
    }

    // Esta funciona redirecciona al usuario si finalizo la partida.
    refrescarVista(estado, etapa) {
        document.querySelector("#etapaDelJuego").innerHTML = etapa;
        if ("finalizado" == estado) self.finalizarJuego();
    }

    finalizarJuego() {
        window.location.href = "/obtenerVistaFinJuego";
    }

    // TODO fusionar con crear_input
    crear_input_stop(respuesta, letter_number) {
        let section_respuestas = document.querySelector("#section-stop");
        let form_respuestas = section_respuestas.querySelector(
            "#form-stop-respuestas"
        );
        let input_enviar = form_respuestas.querySelector(
            "#boton_stop_respuestas"
        );
        let fieldset = PAW.nuevoElemento("fieldset", "", {class: "field_stop_opcion"});
        
        fieldset.appendChild(
            PAW.nuevoElemento("label", respuesta, {
                id: "respuesta_" + String.fromCharCode(letter_number),
                for: "input_stop_"+String.fromCharCode(letter_number)
            })
        );

        fieldset.appendChild(
            PAW.nuevoElemento("input", "", {
                value: String.fromCharCode(letter_number),
                type: "radio",
                name: "respuesta",
                class: "respuesta",
                id: "input_stop_"+String.fromCharCode(letter_number)
            })
        );

        form_respuestas.insertBefore(
            fieldset,
            input_enviar
        );
    }

    verificarTemporizador(segundosJugando, tiempoJuego){
        let time = document.querySelector("#temporizador");
        var minutos_temporizador = parseInt(time.innerText.split(":")[0]);
        var segundos_temporizador = parseInt(time.innerText.split(":")[1]);

        var segundos_jugados = segundosJugando % 60;
        var minutos_jugados = (segundosJugando - segundos_jugados) / 60;

        var minutos_restantes = tiempoJuego - minutos_jugados - 1;
        var segundos_restantes = 60 - segundos_jugados

        if (segundos_restantes == 60)
            segundos_restantes = 59;
        
        segundos_restantes = segundos_restantes.toString();
        minutos_restantes = minutos_restantes.toString();

        if (segundos_restantes.length === 1) {
            segundos_restantes = "0" + segundos_restantes;
        }
    
        if (minutos_restantes.length === 1) {
                minutos_restantes = "0" + minutos_restantes;
        }

        //console.log(minutos_restantes + ":" + segundos_restantes);


        if ((minutos_temporizador == 99) && (segundos_temporizador == 99)){
            time.textContent = minutos_restantes + ":" + segundos_restantes ;
        }
        

        //console.log(minutos);

    }

    actualizarTemporizador() {
        
        let time = document.querySelector("#temporizador");
        var minutos_temporizador = parseInt(time.innerText.split(":")[0]);
        var segundos_temporizador = parseInt(time.innerText.split(":")[1]);

        let min = "";
        let seg = "";

        if (minutos_temporizador === 99 && segundos_temporizador === 99 )
            return;
        
        if (minutos_temporizador === 0 && segundos_temporizador === 0 ) {
            time.textContent = "--:--" ;
            clearInterval(this.ref_tiempo);
        }else{
            if (minutos_temporizador === 0) {
                segundos_temporizador -= 1;
            } else {
                if (segundos_temporizador === 0) {
                    segundos_temporizador = 59;
                    minutos_temporizador -= 1;
                } else {
                    segundos_temporizador -= 1;
                }
            }
            seg = segundos_temporizador.toString();
            min = minutos_temporizador.toString();
    
            if (seg.length === 1) {
                seg = "0" + seg;
            }
    
            if (min.length === 1) {
                min = "0" + min;
            }
    
            time.textContent = min + ":" + seg;    
        }
        
    }

    simularDecrementoBienestar() {
        let medidor = document.querySelector(".medidaBienestar");

        self.bienestar_actual = self.bienestar_actual - 0.3;

        if (self.bienestar_actual < 0) self.bienestar_actual = 0;
        if (self.bienestar_actual > 100) self.bienestar_actual = 100;

        medidor.style.strokeDasharray =
            self.bienestar_actual * 2.73 +
            " " +
            (644 - self.bienestar_actual * 2.72);
        medidor.style.transition = "1s";
    }

    refrescarBienestar(bienestar) {
        // id=medida_bienestar
        self.bienestar_actual = bienestar;
        let medidor = document.querySelector(".medidaBienestar");
        let medidor_mobile = document.getElementById("medida_bienestar");

        if (bienestar < 0) bienestar = 0;
        if (bienestar > 100) bienestar = 100;

        medidor.style.strokeDasharray =
            bienestar * 2.73 + " " + (644 - bienestar * 2.72);
        medidor.style.transition = "1s";

        medidor_mobile.style.height = bienestar * 2;
        medidor_mobile.style.transition = "1s"
    }

    mostrarPantallaInactiva(activo) {
        if (!activo) {
            let pantalla = document.querySelector(".pantalla-inactiva");
            pantalla.classList.remove("ocultar");
        }
    }

    responderPregunta(respuesta,ultimo_elemento_clickeado){
        
        let body = JSON.stringify({
            respuesta: respuesta,
            elemento_asociado: ultimo_elemento_clickeado,
        });

        var myRequest = PAW.armarRequest(
            "/responder",
            body,
            "_token",
            "value"
        );

        fetch(myRequest)
            .then((response) => response.json())
            .then((data) => {
                this.mostrar_correcion(data.justificacion, data.correcta == 1);

                self.efectuarRefreshJuego(data);
            })
            .catch(function (error) {
                console.log(
                    "Hubo un problema con la petición Fetch:" +
                        error.message
                );
            });

        document
            .querySelector(".section-respuestas")
            .classList.add("ocultar");
    }

    cargarStop(data){
        if(!self.mostrar_resultado_stop){
            self.mostrar_resultado_stop = true;
            let form_stop = document.querySelector( "#form-stop-respuestas");
            // form_stop.querySelectorAll(".respuesta")
            //     .forEach((child) =>
            //         form_stop.removeChild(child)
            //     );
            form_stop.querySelectorAll(".field_accion_opcion")
                .forEach((child) =>
                    form_stop.removeChild(child)
                );
            let section_respuestas_stop = document.querySelector("#section-stop");
            section_respuestas_stop.querySelector( "#enunciado" ).innerHTML = data.enunciadoStop;
    
            let letter_number = 97;
            data.opciones.forEach((respuesta) =>
                // TODO Refactorizar para que crear_input se fusione con crear_input_stop
                self.crear_input_stop(
                    respuesta,
                    letter_number++
                )
            );
            section_respuestas_stop.classList.remove("ocultar");
        }
    }

    responderPreguntaStop(respuesta)
    {
        console.log("respuesta enviada: < " + respuesta + " >");
        
        let body = JSON.stringify({
            respuesta: respuesta,
        });

        var myRequest = PAW.armarRequest(
            "/responder_stop",
            body,
            "_token",
            "value"
        );

        // aca solamente se envia la respuesta, y muestra un cartel de "esperando respuesta"
        document.getElementById("section-stop").classList.add("ocultar");
        document.getElementById("espera").classList.remove("ocultar");

        fetch(myRequest)
            .then((response) => response.json())
            .then((data) => {
                console.log(data)
            })
            .catch(function (error) {
                console.log(
                    "Hubo un problema con la petición Fetch:" +
                        error.message
                );
            });
    }

    cargarResultadoStop(data)
    {
        if(self.mostrar_resultado_stop && (data.estado == "jugando")){
            self.mostrar_resultado_stop = false;
            document.getElementById("espera").classList.add("ocultar");
            let ventana_resultado = document.getElementById("justificacion-stop");

            // arma la ventana con los datos necesarios
            //TODO quita todos los hijos, menos el boton, del cartel 
            ventana_resultado.classList.remove("resultado_correcto");
            ventana_resultado.classList.remove("resultado_incorrecto");

            // arma el h3, que dira si acertaron o no, y agrega la clase css
            let mensaje;
            if(data.correcto){
                mensaje = "¡Han acertado!";
                ventana_resultado.classList.add("resultado_correcto");
            }else{
                mensaje = "No han acertado...";
                ventana_resultado.classList.add("resultado_incorrecto");
            }
            let mensaje_h3 = PAW.nuevoElemento("h4", mensaje, {
                id: "mensaje_stop"
            });
            
            // Arma el texto que indica la respuesta correcta de la pregunta
            let respuesta = PAW.nuevoElemento("p", "La respuesta correcta es " + data.respuesta_Optima, {
                id: "respuesta_optima_stop"
            });

            // Arma la justificacion, que explica la respuesta
            let justificacion = PAW.nuevoElemento("p", data.justificacion, {
                id: "justificacion_stop"
            });

            ventana_resultado.insertBefore(mensaje_h3, ventana_resultado.lastElementChild);
            ventana_resultado.insertBefore(respuesta, ventana_resultado.lastElementChild);
            ventana_resultado.insertBefore(justificacion, ventana_resultado.lastElementChild);

            ventana_resultado.classList.remove("ocultar");
        }
    }

    resize() {
        document.getElementById("mapa_principal").style.zoom = self.zoom+'%';
    }

    iniciarTimer() 
    {
        this.ref_bienestar = setInterval(this.simularDecrementoBienestar, 1000);
    }
    
    pararTimer() 
    {
        clearInterval(this.ref_bienestar);
    }

}
