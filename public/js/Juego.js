class Juego {
    constructor() {
        //Inicialización de funcioalidades
        document.addEventListener("DOMContentLoaded", () => {
            PAW.cargarScript("vistaGameplay", "/js/vistaGameplay.js", () => {
                let gameplay = new vistaGameplay();
            });
        });
    }
}

let app = new Juego();
