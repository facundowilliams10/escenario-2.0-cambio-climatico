class PAW {
    //nuevoElemento("script", "", {scr: URL, name: "nombreDelScript"})
    static nuevoElemento(tag, contenido, atributos = {}) {
        let elemento = document.createElement(tag);

        for (const atributo in atributos) {
            elemento.setAttribute(atributo, atributos[atributo]);
        }
        if (contenido.tagName) elemento.appendChild(contenido);
        else elemento.appendChild(document.createTextNode(contenido));

        return elemento;
    }

    static cargarScript(nombre, url, fnCallback = null) {
        let elemento = document.querySelector("script#" + nombre);
        if (!elemento) {
            //Creo el tag script
            elemento = this.nuevoElemento("script", "", {
                src: url,
                id: nombre,
            });

            //Funcion de Callback
            if (fnCallback) elemento.addEventListener("load", fnCallback);

            document.head.appendChild(elemento);
        }

        return elemento;
    }

    /* */
    /* Funcion utilizada para armar una plantilla para las peticiones que se realizan al servidor */
    /* */

    static armarRequest(url, body, token, token_value) {
        var csrf_token = document
            .getElementsByName(token)[0]
            .getAttribute(token_value);

        var myHeaders = new Headers();

        myHeaders.append("X-CSRF-TOKEN", csrf_token);
        myHeaders.append("Content-type", "application/json; charset=UTF-8");

        var myInit = {
            method: "POST",
            headers: myHeaders,
            mode: "cors",
            cache: "default",
            body: body,
        };

        return new Request(url, myInit);
    }
    
    //obtiene la coocke por el nombre
    static getCookie(cName) {
        const name = cName + "=";
        const cDecoded = decodeURIComponent(document.cookie);
        const cArr = cDecoded.split('; ');
        let res;
        cArr.forEach(val => {
          if (val.indexOf(name) === 0) res = val.substring(name.length);
        })
        return res
    }
}
