class Aplicacion {
    pantalla_inactiva;
    cuerpo;
    rec_func;
    self;
    mensajeRotarPantalla;
    intervaloMensajeRotarPantalla;

    constructor() {
        self = this;
        document.addEventListener("DOMContentLoaded", () => {

            self = this;
            this.cuerpo = document.querySelector(".main_body");
            this.pantalla_inactiva =
                document.querySelector("#pantalla-inactiva");
            let mensajeRotarPantalla =
                document.querySelector(".rotar-pantalla");
            let ventana_buscador = document.querySelector(".buscadorSalas");
            let boton_cerrar_buscador = document.getElementById(
                "boton_cerrar_buscador"
            );

            let boton_buscador = document.getElementById("boton_buscador");
            let ventana_reglamento = document.querySelector(".reglamentoJuego");
            let ventana_sound = document.querySelector(".sound_config");
            let ventana_crear = document.querySelector(".creadorSalas");
            let sound_slider = document.getElementById("sound-slider");
            let boton_crear = document.getElementById("boton_crear");
            let boton_cerrar_validacion = document.getElementById("boton_cerrar_errors");

            self.mensajeSalasNoDisponibles = "";
            this.verificarYObtenerMensajeSalasNoDisponibles();

            //Si el dispositivo está en modo horizontal muestramos el mensaje
            if ( (matchMedia('all and (orientation:landscape)').matches) ) 
                mensajeRotarPantalla.classList.add("no-mostrar");
    
            //Si el dispositivo cambia la orientación mostramos el mensaje si no es horizontal
            window.addEventListener("orientationchange", function(event) {
                if ( event.target.screen.orientation.angle ==90 ){
                    mensajeRotarPantalla.classList.add("no-mostrar");
                }
                else
                    mensajeRotarPantalla.classList.remove("no-mostrar");
            });

            // Boton Creador
            document
                .getElementById("boton_crear")
                .addEventListener("click", () => {
                    ventana_crear.classList.remove("pop_up_hid");
                    this.deshabilitar_cuerpo();
                });

            // Boton cerrar creador
            if (document.getElementById("boton_cerrar_creador"))
                document
                    .getElementById("boton_cerrar_creador")
                    .addEventListener("click", () => {
                        ventana_crear.classList.add("pop_up_hid");
                        this.habilitar_cuerpo();
                    });
            // Boton Cerrar Buscador
            boton_cerrar_buscador.addEventListener("click", () => {
                clearInterval(this.rec_func);
                ventana_buscador.classList.add("pop_up_hid");
                this.habilitar_cuerpo();
            });

            // Boton Buscador
            boton_buscador.addEventListener("click", () => {
                // Recuperar salas e imprimirlas en la lista
                this.recuperar_salas();
                this.rec_func = setInterval(this.recuperar_salas, 2000);
                ventana_buscador.classList.remove("pop_up_hid");
                this.deshabilitar_cuerpo();
                self.mensajeSalasNoDisponibles = "";
                this.verificarYObtenerMensajeSalasNoDisponibles();
            });

            // Boton Reglamento
            if (document.getElementById("boton_reglamento"))
                document
                    .getElementById("boton_reglamento")
                    .addEventListener("click", () => {
                        ventana_reglamento.classList.remove("pop_up_hid");
                        this.deshabilitar_cuerpo();
                    });
            boton_crear.addEventListener("click", () => {
                ventana_crear.classList.remove("pop_up_hid");
                this.deshabilitar_cuerpo();
            });

            // Boton Cerrar Reglamento
            if (document.getElementById("boton_cerrar_reglamento"))
                document
                    .getElementById("boton_cerrar_reglamento")
                    .addEventListener("click", () => {
                        ventana_reglamento.classList.add("pop_up_hid");
                        this.habilitar_cuerpo();
                    });

            // Video Icon (para volver a ver el video)
            if (document.getElementById("video_icon"))
                document
                    .getElementById("video_icon")
                    .addEventListener("click", () => {
                        window.location.replace("/inicio");
                    });

            // Sound Icon (para abrir el pop-up de sonido)
            if (document.getElementById("sound_icon"))
                document
                    .getElementById("sound_icon")
                    .addEventListener("click", () => {
                        ventana_sound.classList.remove("pop_up_hid");
                        this.deshabilitar_cuerpo();
                    });

            // Pop-up Sound boton (para cerrar el pop-up)
            if (document.getElementById("boton_cerrar_sound"))
                document
                    .getElementById("boton_cerrar_sound")
                    .addEventListener("click", () => {
                        ventana_sound.classList.add("pop_up_hid");
                        this.habilitar_cuerpo();
                    });

            // Elemento Slider de Sonido
            if (sound_slider)
                sound_slider.addEventListener("input", () => {
                    document.getElementById("background_music").volume =
                        sound_slider.value / 100;
                });

            // Boton para cerrar el cartel de validacion
            if(boton_cerrar_validacion){
                this.deshabilitar_cuerpo();
                boton_cerrar_validacion.addEventListener("click", () => {
                    document.getElementById("error_validacion").classList.add("pop_up_hid");
                    this.habilitar_cuerpo();
                });
            }
        });
    }

    verificarYObtenerMensajeSalasNoDisponibles() {
        if (self.mensajeSalasNoDisponibles == "") {
            try {
                self.mensajeSalasNoDisponibles = document.getElementById(
                    "salas_no_disponibles"
                ).innerText;
            } catch (error) {}
        }
    }

    deshabilitar_cuerpo() {
        //this.cuerpo.classList.add("disabled");
        this.pantalla_inactiva.classList.remove("ocultar");
    }

    habilitar_cuerpo() {
        // this.cuerpo.classList.remove("disabled");
        this.pantalla_inactiva.classList.add("ocultar");
    }

    /* */
    /* Comienzo de bloque de metodos relaciondos con mostrar las salas disponibles. */
    /* */

    recuperar_salas() {
        let lista_salas = document.getElementById("lista_Salas");
        fetch("/obtenerSalas")
            .then((response) => response.json())
            .then((salas) => {
                // Limpiar la lista actual, porque se ponen uno debajo de otro

                if (salas.length > 0) {
                    lista_salas.textContent = "";
                    salas.forEach((sala) => {
                        var item_sala = self.crearElementoSala(sala);

                        if (item_sala) {
                            lista_salas.appendChild(item_sala);

                            // Agregar evento a cada item
                            item_sala.addEventListener("click", () => {
                                self.cambiarSalaSeleccionada(item_sala);
                            });
                        }
                    });
                } else {
                    lista_salas.textContent = "";
                    lista_salas.appendChild(
                        PAW.nuevoElemento(
                            "li",
                            self.mensajeSalasNoDisponibles,
                            {
                                id: "salas_no_disponibles",
                            }
                        )
                    );
                }
            })
            .catch(function (error) {
                console.log(
                    "Hubo un problema con la petición Fetch:" + error.message
                );
            });
    }

    crearElementoSala(sala) {
        //if (sala.estado == "espera") { //Todas las salas que vienen del back ahora, son salas en espera.
        // TODO Mostrar salas que no estén en espera.
        let item_sala = PAW.nuevoElemento("li", "", {
            class: "item_sala",
        });

        self.crearSubelementosSala(sala, item_sala);

        // Se crea el boton de eliminar sala, si es anfitrion de la sala.
        if (sala.is_anfi) self.crearSubelementosAnfitrion(item_sala);

        // Me fijo si el item_sala que estoy agregando, es la sala seleccionada, para aplicar los estilos.
        if (sala.id == document.getElementById("sala_seleccionada").value) {
            item_sala.classList.add("item_sala_seleccionado");
        }
        return item_sala;
        // }
    }

    crearSubelementosSala(sala, item_sala) {
        let inp_sala = PAW.nuevoElemento("input", "", {
            type: "hidden",
            class: "input_hid_sala",
            value: sala.id,
        });
        let nombre_sala = PAW.nuevoElemento(
            "p",
            sala.nombre +
                " (" +
                sala.player_count +
                "/" +
                sala.numJugadores * 2 +
                ") " +
                sala.dificultad,
            { class: "nombre_sala" }
        );

        item_sala.appendChild(inp_sala);
        item_sala.appendChild(nombre_sala);
    }

    crearSubelementosAnfitrion(item_sala) {
        let icon_kick = PAW.nuevoElemento("span", "", {
            class: "kick_icon",
        });
        icon_kick.addEventListener("click", () => {
            self.eliminar_sala(icon_kick);
        });
        item_sala.appendChild(icon_kick);
    }

    cambiarSalaSeleccionada(item_sala) {
        // Le saco la clase de item_sala_seleccionado al item_sala anterior, si es que hay alguno seleccionado.
        let sala_anterior_seleccionada = document.querySelector(
            ".item_sala_seleccionado"
        );
        if (sala_anterior_seleccionada != null) {
            sala_anterior_seleccionada.classList.remove(
                "item_sala_seleccionado"
            );
        }

        // Indicamos como sala seleccionada del input hidden, a la que se acaba de clickear.
        sala_seleccionada.value =
            item_sala.querySelector(".input_hid_sala").value;

        // Cuando el usuario haga click, le agrego al item_sala, la clase item_sala_seleccionado
        // Para que se apliquen los estilos.
        item_sala.classList.add("item_sala_seleccionado");
    }

    mostrarMensajeRotarPantalla(){
        if ( (matchMedia('all and (orientation:landscape)').matches) ) {
            this.mensajeRotarPantalla.classList.add("ocultar");
            //clearInterval(this.intervaloMensajeRotarPantalla);
        }
    }

    /* */
    /* Fin de bloque de metodos relaciondos con mostrar las salas disponibles. */
    /* */

    // Eliminar sala cuando anfitrion toque el boton (x) de la lista de salas.
    eliminar_sala(btn_eliminar) {
        let contenedor_item = btn_eliminar.parentElement;
        let input_sala = contenedor_item.querySelector(".input_hid_sala");
        let id_sala = input_sala.value;

        let body = JSON.stringify({
            id_sala: id_sala,
        });

        var myRequest = PAW.armarRequest(
            "/eliminarSala",
            body,
            "_token",
            "value"
        );

        fetch(myRequest)
            .then((response) => response.json())
            .then((response) => {})
            .catch(function (error) {
                console.log(
                    "Hubo un problema con la petición Fetch:" + error.message
                );
            });
        
    }
}

let App = new Aplicacion();
