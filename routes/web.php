<?php

use App\Http\Controllers\EquiposController;
use App\Http\Controllers\LanguageController;
use App\Models\Pregunta;
use App\Models\Respuesta;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegistroController;
use App\Http\Controllers\SessionsController;
use App\Http\Controllers\PreguntasController ;
use App\Http\Controllers\SalasController ;
use App\Http\Controllers\PartidasController ;
use App\Http\Controllers\UpdateController;

/*
|--------------------------------------------------------------------------
|                                   Rutas Web
|--------------------------------------------------------------------------
| Aca se registran las rutas del juego. Estas rutas son cargadas por RouteServiceProvider
| dentro de un grupo que contiene el middleware "web"
*/

Route::get('/', function () {
    return redirect('/login');
})->middleware('guest');

Route::get('/tutorial', function () {
    return view('inicio');
});

Route::get('/inicio', function () {
    return view('inicio');
});

/* */
/* */
/* */

Route::get('/preguntas/{pregunta}', function (Pregunta $pregunta) {

    //Se encuentra la pregunta solicitada y se pasa a la vista "pregunta"
    return view(
        'preguntas/pregunta',
        [
        'pregunta' => $pregunta,
        'respuestas' => $pregunta->respuestas->load('pregunta')//carga todas las respuestas de esa pregunta en una query
    ]
    );
})
// Por seguridad y para evitar que se inyecte codigo en la ruta,
// solo se permiten caracteres alfanumericos:
->whereAlphaNumeric('pregunta');
Route::get('/getPregunta/{elemento_asociado}', [PreguntasController::class, 'getPregunta'])->middleware('auth');
Route::post('/responder', [PreguntasController::class, 'responder'])->middleware('auth');
Route::post('/cargarPreguntaStop', [PreguntasController::class, 'cargarPreguntaStop'])->middleware('auth');
Route::get('admin/preguntas/create', [PreguntasController::class, 'create'])->middleware('auth');;//->middleware('admin');
Route::post('admin/preguntas', [PreguntasController::class, 'store'])->middleware('auth');;//->middleware('admin');
Route::get('/getStop/{id_sala}', [PreguntasController::class, 'getStop'])->middleware('auth');
Route::post('/responder_stop', [PreguntasController::class, 'responder_stop'])->middleware('auth');

/* */
/* */
/* */

Route::get('registro', [RegistroController::class, 'create'])->middleware('guest');
Route::post('registro', [RegistroController::class, 'store'])->middleware('guest');

/* */
/* */
/* */

Route::get('/salas', function () {
    return view('salas');
})->middleware('auth')->name("salas");
Route::get('/creadorSalas', function () {
    return view('creadorSalas');//TODO validar que sea admin
})->middleware('auth')->name("creadorSalas");
Route::post('/crearSala', [SalasController::class, 'create'])->middleware('auth')->name("salas.create");
Route::post('/unirseSala', [SalasController::class, 'join'])->middleware('auth')->name("salas.join");
Route::get('/obtenerSalas', [SalasController::class, 'obtenerSalas'])->middleware('auth')->name("salas.get");
Route::post('/actualizarSala', [SalasController::class, 'actualizarSala'])->middleware('auth')->name("salas.update");
Route::post('/salirSala', [SalasController::class, 'salirSala'])->middleware('auth')->name("salas.exit");
Route::post('/eliminarSala', [SalasController::class, 'eliminarSala'])->middleware('auth')->name("salas.delete");
Route::post('/finalizarSala', [SalasController::class, 'finalizarSala'])->middleware('auth')->name("salas.delete");
Route::get('/refrescarSala', [SalasController::class, 'refrescarSala'])->middleware('auth')->name("salas.refresh");

/* Rutas que pase de partida */

Route::post('/iniciarJuego', [SalasController::class, 'iniciar'])->middleware('auth')->name("game.start");
Route::get('/refrescarJuego', [SalasController::class, 'refrescarJuego'])->middleware('auth')->name("game.refresh");
Route::get('/gameplay', [SalasController::class, 'obtenerVistaJuego'])->middleware('auth')->name("game.obtain");
Route::get('/obtenerVistaFinJuego', [SalasController::class, 'obtenerVistaFinJuego'])->middleware('auth')->name("game.end");


/* */
/* */
/* */

Route::post('logout', [SessionsController::class, 'destroy'])->middleware('auth');
//Route::post('/admin/preguntas/create', [SessionsController::class, 'destroy'])->middleware('auth');
Route::post('sessions', [SessionsController::class, 'store'])->middleware('guest');
Route::get('login', [SessionsController::class, 'create'])->middleware('guest')->name('login')->middleware('guest');

/* */
/* */
/* */

Route::post('/kickearJugador', [EquiposController::class, 'kickearJugador'])->middleware('auth');
Route::post('/moverJugador', [EquiposController::class, 'moveJugador'])->middleware('auth');

/* */
/* */
/* */

Route::get('language-change', [LanguageController::class, 'changeLanguage'])->name('changeLanguage');

/* */
/* */
/* */



/* Delete this */
Route::get("update", [UpdateController::class, 'update'])->name('update');
Route::get("migration", [PreguntasController::class, "migration"])->name("migration");
Route::get("databaseDump", [PreguntasController::class, "databaseDump"])->name("databaseDump");
