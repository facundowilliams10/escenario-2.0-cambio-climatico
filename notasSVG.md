#Convenciones para el desarrollo de la pagina del gameplay

-Identificaremos a las imagenes dandole un ID al <svg>, para obtener de forma unívoca a un objeto específico (por ejempplo, icono_pregunta_1)

-Le daremos clases a las <svg> con atributos o comportamientos comunes. Por ejemplo, la clase iconos de pregunta, que tienen atributos en comun como el tamaño.

-las imagenes SVG QUE ESTAN RELACIONADAS las insertaremos dentro de un <section>'s (por ejemplo todas las imagenes de termometros dentro del section termometro)

-Para cambiar el tamaño del svg, es decir escalar, utilizaremos transform="scale(0.1)" dentro de la tag <g>