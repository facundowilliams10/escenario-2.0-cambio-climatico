<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccionRealizadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accion_realizadas', function (Blueprint $table) {
            $table->id();
            $table->timestamp('done_at', $precision = 0);
            $table->string('elemento_asociado', 100);
            $table->boolean("is_optima");
            $table->timestamps();
            $table->string('respuesta', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accion_realizadas');
    }
}
