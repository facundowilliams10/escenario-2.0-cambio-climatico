<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preguntas', function (Blueprint $table) {
            $table->id("id");
            $table->string('elemento_asociado', 100);
            $table->timestamps();
            $table->string('enunciado')->unique();
            $table->enum('dificultad', ['facil', 'medio', 'dificil']);
            $table->text('justificacion');
            $table->text('language');
            $table->enum('etapa', ['maniana', 'mediodia', 'tarde']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preguntas');
    }
}
