<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salas', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('nombre');
            $table->integer('tiempoJuego');
            $table->integer('numJugadores');
            $table->string('dificultad');
            $table->double('temperaturaGlobal', 5, 2);
            //$table->time('tiempoTranscurrido');
            $table->timestamp('timestampCreacionSala')->nullable()->default(null);
            $table->timestamp('timestampInicioJuego')->nullable()->default(null);
            $table->timestamp('timestampUltimoRefresh')->nullable()->default(null);
            $table->enum('etapaActual', ['maniana', 'mediodia', 'tarde']);
            $table->enum('estado', ['espera', 'jugando', 'finalizado', 'stop', 'stop_contestado']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salas');
    }
}
