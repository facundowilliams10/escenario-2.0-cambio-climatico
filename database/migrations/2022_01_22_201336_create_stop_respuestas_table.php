<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStopRespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stop_respuestas', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('texto');
            $table->boolean('isOptima');//Si es optima debería restar grados, sino debería sumar grados 
            $table->unsignedBigInteger('stop_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stop_respuestas');
    }
}
