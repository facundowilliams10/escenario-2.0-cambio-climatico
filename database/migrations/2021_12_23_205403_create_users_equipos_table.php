<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_equipos', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('bienestar')->default(100);
            $table->integer('gradosGenerados')->default(0);
            $table->enum('estado', ['activo', 'inactivo']);
            $table->timestamp('tiempoTranscurridoDecrementoBienestar')->nullable()->default(null);
            $table->unsignedBigInteger('equipo_id');
            $table->foreign('equipo_id')->references('id_equipo')->on('equipos')->onDelete("cascade")->onUpdate("cascade");
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_equipos');
    }
}
