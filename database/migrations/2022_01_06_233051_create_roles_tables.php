<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            //
            $table->bigIncrements('id');
            $table->string('nombre');// ej: profesor, alumno
            //El siguiente atributo es opcional y sirve para separar el nombre del rol con la etiqueta que le mostramos al usuario
            $table->string('label')->nullable();// ej: profesor del curso, alumno de segundo año
            $table->timestamps();   
        });

        Schema::create('permisos', function (Blueprint $table) {
            //
            $table->bigIncrements('id');
            $table->string('nombre'); //ej: cargarPreguntas, crearSalas
            //El siguiente atributo es opcional y sirve para separar el nombre del rol con la etiqueta que le mostramos al usuario
            $table->string('label')->nullable();//ej: Puede cargar preguntas, puede crear salas de juego
            $table->timestamps();   
        });

        Schema::create('permiso_rol', function (Blueprint $table) {
            
            //La PK es la combinación de los ids de las tablas que relaciona esta tabla n a n
            $table->primary(['rol_id', 'permiso_id']);
            $table->unsignedBigInteger('rol_id');
            $table->unsignedBigInteger('permiso_id');
            $table->timestamps();

            $table->foreign('rol_id')
                ->references('id')
                ->on('roles')
                ->onDelete('cascade');//Si se borra el rol asociado se borran también en cascada las intancias de la tabla permiso_rol

            $table->foreign('permiso_id')
            ->references('id')
            ->on('permisos')
            ->onDelete('cascade');//Si se borra el rol asociado se borran también en cascada las intancias de la tabla permiso_rol
        });

        Schema::create('user_rol', function (Blueprint $table) {
            
            //La PK es la combinación de los ids de las tablas que relaciona esta tabla n a n
            $table->primary(['rol_id', 'user_id']);
            $table->unsignedBigInteger('rol_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('rol_id')
                ->references('id')
                ->on('roles')
                ->onDelete('cascade');//Si se borra el rol asociado se borran también en cascada las intancias de la tabla permiso_rol

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_rol');
        Schema::dropIfExists('permiso_rol');
        Schema::dropIfExists('permisos');
        Schema::dropIfExists('roles');
    }
}
